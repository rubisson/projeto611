/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.ct.dainf.if62c.pratica;

import java.util.HashMap;

/**
 *
 * @author Rubinho
 */
public class Time {
    private HashMap<String,Jogador> jogadores = new HashMap<>();
    private Jogador j1;

    public Time(){
    }
    public Time(Integer numero,String nome){
        j1 = new Jogador(numero,nome);
    }

    public HashMap<String, Jogador> getJogadores() {
        return jogadores;
    }

    
    public void addJogadores(String posicao, Jogador j1){
        jogadores.put(posicao, j1);
    }
}
